﻿using Repositories.Contract;
using Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BAL
{
    public class LoginBAL
    {
        #region Global Variables
        ILoginRepository _loginRepo;
        #endregion

        #region UserBAL
        public LoginBAL()
        {
            _loginRepo = new LoginRepository();
        }
        #endregion

        #region Validate Credentials
        public bool ValidateCredentials(Entities.Login login,out string employeeId,out string employeeName)
        {
           return _loginRepo.ValidateCredentials(login,out employeeId,out employeeName);
        }
        #endregion
    }
}
