﻿using Common.Helpers;
using Repositories.Contract;
using Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BAL
{
    public class PaymentFileDetailsBAL
    {
        #region Global Variables
        IPaymentFileDetaills _payRep;
        #endregion

        #region Constructor
        public PaymentFileDetailsBAL()
        {
            _payRep = new PaymentFileDetailsRepository();
        }
        #endregion

        #region GetList
        public List<object> GetList(GridSearchParameters param, int chequeRunNumber)
        {
            return _payRep.GetList(param, chequeRunNumber);
        }
        #endregion
    }
}
