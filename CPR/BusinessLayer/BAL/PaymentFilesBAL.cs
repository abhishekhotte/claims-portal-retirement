﻿using Common.Helpers;
using Repositories.Contract;
using Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BAL
{
    public class PaymentFilesBAL
    {
        #region Global Variables
        IPaymentFiles _payRep;
        #endregion

        #region Constructor
        public PaymentFilesBAL()
        {
            _payRep = new PaymentFilesRepository();
        }
        #endregion

        #region GetList
        public List<object> GetList(GridSearchParameters param)
        {
            return _payRep.GetList(param);
        }
        #endregion
    }
}
