﻿using Common.Helpers;
using Repositories.Contract;
using Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BAL
{
    public class SendToBankBAL
    {
        #region Global Variables
        ISendToBank _bankRep;
        #endregion

        #region Constructor
        public SendToBankBAL()
        {
            _bankRep = new SendToBankRepository();
        }
        #endregion

        #region GetList
        public List<object> GetList(GridSearchParameters param, int chequeRunNumber)
        {
            return _bankRep.GetList(param, chequeRunNumber);
        }
        #endregion

        #region To cancel an individual payment request which has been exported to Bank
        public bool CancelPaymentRequest(int id)
        {
            return _bankRep.CancelPaymentRequest(id);
        }
        #endregion

        #region To complete an individual payment request which has been exported to Bank
        public bool CompletePaymentRequest(int id, string completedBy, string bankingAppRef)
        {
            return _bankRep.CompletePaymentRequest(id, completedBy, bankingAppRef);
        }
        #endregion

        #region To export an individual payment request and send to Bank
        public bool ExportPaymentRequest(int id)
        {
            return _bankRep.ExportPaymentRequest(id);
        }
        #endregion

        #region To regenerate an individual payment request which has already been sent to Bank
        public bool RegeneratePaymentRequest(int id)
        {
            return _bankRep.RegeneratePaymentRequest(id);
        }
        #endregion
    }
}
