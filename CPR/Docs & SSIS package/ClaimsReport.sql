
USE [ClaimsPortal]
GO
/****** Object:  Table [dbo].[FinancialInstitutionInformation]    Script Date: 06/21/2017 17:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FinancialInstitutionInformation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](256) NOT NULL,
	[AccountNumber] [varchar](64) NOT NULL,
	[SortCode] [varchar](64) NOT NULL,
	[PaymentTransactionID] [int] NOT NULL,
	[Payer] [bit] NOT NULL,
 CONSTRAINT [PK_FinancialInstitutionInformation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClaimsAuditLog]    Script Date: 06/21/2017 17:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClaimsAuditLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PaymentTransactionID] [int] NOT NULL,
	[TransactionDateandTime] [datetime] NOT NULL,
	[Status] [nvarchar](64) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClaimDetails]    Script Date: 06/21/2017 17:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClaimDetails](
	[PaymentFileID] [int] NOT NULL,
	[PaymentTransactionID] [int] NOT NULL,
	[ClaimReference] [varchar](64) NOT NULL,
	[Currency] [char](3) NOT NULL,
	[TotalAmount] [money] NOT NULL,
	[PayeeName] [varchar](64) NOT NULL,
	[PayeeReference] [varchar](64) NOT NULL,
	[DateOfIssue] [datetime] NOT NULL,
	[Status] [varchar](42) NOT NULL,
	[BulkTransaction] [bit] NOT NULL,
	[PaymentMethod] [varchar](32) NOT NULL,
	[RequestedDate] [datetime] NOT NULL,
	[TransactionReason] [varchar](max) NULL,
	[chequePayeeFullName] [varchar](64) NULL,
	[InsertedToClaimsPortal] [bit] NULL,
	[InsertedToClaimsPortalDate] [datetime] NULL,
	[InsertedToClaimsPortalID] [int] NULL,
 CONSTRAINT [PK_ClaimDetails] PRIMARY KEY CLUSTERED 
(
	[PaymentFileID] ASC,
	[PaymentTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressDetails]    Script Date: 06/21/2017 17:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressDetails](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[NameLine1] [varchar](64) NOT NULL,
	[NameLine2] [varchar](64) NULL,
	[StreetLine1] [varchar](256) NOT NULL,
	[StreetLine2] [varchar](256) NULL,
	[Town] [varchar](64) NOT NULL,
	[country] [varchar](64) NOT NULL,
	[postcode] [varchar](64) NOT NULL,
	[isoCountryCode] [varchar](64) NOT NULL,
	[PaymentTransactionID] [int] NOT NULL,
	[Payer] [bit] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountDetails]    Script Date: 06/21/2017 17:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountDetails](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](64) NOT NULL,
	[AccountNumber] [int] NOT NULL,
	[sortCode] [varchar](64) NOT NULL,
	[PaymentTransactionID] [int] NOT NULL,
	[Payer] [bit] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
