﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Contract
{
    public interface ILoginRepository
    {
        bool ValidateCredentials(Login loginModel, out string employeeId, out string employeeName);
    }
}
