﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Contract
{
    public interface IPaymentFiles
    {
        List<object> GetList(GridSearchParameters param);
    }
}
