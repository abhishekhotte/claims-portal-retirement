﻿using Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Contract
{
    public interface ISendToBank
    {
        List<object> GetList(GridSearchParameters param, int chequeRunNumber);
        bool CancelPaymentRequest(int id);
        bool CompletePaymentRequest(int id, string completedBy, string bankingAppRef);
        bool ExportPaymentRequest(int id);
        bool RegeneratePaymentRequest(int id);
    }
}
