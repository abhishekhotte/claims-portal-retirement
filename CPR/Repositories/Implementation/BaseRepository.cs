﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Implementation
{
    public class BaseRepository
    {
        #region Global Variables
        private ClaimsPortalRetirementEntities _db;
        #endregion

        #region dbContext
        public ClaimsPortalRetirementEntities dbContext
        {
            get
            {
                if (_db == null)
                {
                    _db = new ClaimsPortalRetirementEntities();
                }
                return _db;
            }
        }
        #endregion
    }
}
