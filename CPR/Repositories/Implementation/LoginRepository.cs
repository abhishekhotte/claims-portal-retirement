﻿using Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Repositories.Implementation
{
    public class LoginRepository : ILoginRepository
    {
        string payrollSqlConSstring = ConfigurationManager.ConnectionStrings["MCEPayrollConnectionString"].ConnectionString;
        SqlConnection con=null;
        SqlDataAdapter da = null;
        DataSet ds = null;

        public bool ValidateCredentials(Login loginModel, out string employeeId, out string employeeName)
        {
            string status = string.Empty, empId=string.Empty,empName=string.Empty;
            con = new SqlConnection(payrollSqlConSstring);
            da = new SqlDataAdapter("ValidateCredentials", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add(new SqlParameter
            {
                ParameterName = "@username",
                Value = loginModel.LoginId,
                SqlDbType = SqlDbType.NVarChar,
                Size = 1000  // Assuming a 2000 char size of the field annotation (-1 for MAX)
            });

            DataSet ds = new DataSet();
            da.Fill(ds);
            status = ds.Tables[0].Rows[0][0].ToString();
            if (status == "Success")
            {
                employeeId = ds.Tables[0].Rows[0]["EmployeeId"].ToString();
                employeeName = ds.Tables[0].Rows[0]["Name"].ToString();
                return true;
            }
            else
            {
                employeeId = employeeName = null;
                return false;
            }
        }
    }
}
