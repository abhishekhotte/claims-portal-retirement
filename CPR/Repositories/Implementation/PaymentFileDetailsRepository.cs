﻿using Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Helpers;

namespace Repositories.Implementation
{
    public class PaymentFileDetailsRepository : BaseRepository, IPaymentFileDetaills
    {
        public List<object> GetList(GridSearchParameters param, int chequeRunNumber)
        {
            List<object> list = new List<object>();
            GridHelper<paymentFileDetails_GetList_Result> fileList = new GridHelper<paymentFileDetails_GetList_Result>();
            List<paymentFileDetails_GetList_Result> lstAllPaymentFileDetails = dbContext.paymentFileDetails_GetList(chequeRunNumber, param.filterBy,
                Convert.ToInt32(param.currentPage), Convert.ToInt32(param.pageItems), param.orderBy, param.orderByReverse).ToList();
            fileList.ListOfItems = lstAllPaymentFileDetails;
            if (lstAllPaymentFileDetails.Count > 0)
                fileList.TotalRecordCount = (int) lstAllPaymentFileDetails[0].TotalCount;
            list.Add(fileList);
            return list;
        }
    }
}
