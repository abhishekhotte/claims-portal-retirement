﻿using Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Helpers;

namespace Repositories.Implementation
{
    public class PaymentFilesRepository : BaseRepository, IPaymentFiles
    {
        public List<object> GetList(GridSearchParameters param)
        {
            List<object> list = new List<object>();
            GridHelper<paymentFiles_GetList_Result> fileList = new GridHelper<paymentFiles_GetList_Result>();
            List<paymentFiles_GetList_Result> lstAllUsers = dbContext.paymentFiles_GetList(param.filterBy,
                Convert.ToInt32(param.currentPage), Convert.ToInt32(param.pageItems), param.orderBy, param.orderByReverse).ToList();
            fileList.ListOfItems = lstAllUsers;
            if (lstAllUsers.Count > 0)
                fileList.TotalRecordCount = (int) lstAllUsers[0].TotalCount;
            list.Add(fileList);
            return list;
        }
    }
}
