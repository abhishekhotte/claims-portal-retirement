﻿using Common.Helpers;
using Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Implementation
{
    public class SendToBankRepository : BaseRepository, ISendToBank
    {
        public List<object> GetList(GridSearchParameters param, int chequeRunNumber)
        {
            List<object> list = new List<object>();
            GridHelper<sendToBank_GetList_Result> fileList = new GridHelper<sendToBank_GetList_Result>();
            List<sendToBank_GetList_Result> lstAllTransactionDetails = dbContext.sendToBank_GetList(chequeRunNumber,param.filterBy, Convert.ToInt32(param.currentPage), Convert.ToInt32(param.pageItems), param.orderBy, param.orderByReverse).ToList();
            fileList.ListOfItems = lstAllTransactionDetails;
            if (lstAllTransactionDetails.Count > 0)
            {
                fileList.TotalRecordCount = (int) lstAllTransactionDetails[0].TotalCount;
                list.Add(fileList);
                list.Add(new
                            {
                                EURCurrencyTotal = lstAllTransactionDetails[0].EURCurrencyTotal,
                                GBPCurrencyTotal = lstAllTransactionDetails[0].GBPCurrencyTotal
                            });
            }
            return list;
        }

        public bool CancelPaymentRequest(int id)
        {
            return dbContext.cancelPaymentRequest(id) > 0;
        }

        public bool CompletePaymentRequest(int id, string completedBy, string bankingAppRef)
        {
            return dbContext.completePaymentRequest(id, completedBy, bankingAppRef) > 0;
        }

        public bool ExportPaymentRequest(int id)
        {
            return dbContext.exportPaymentRequest(id) > 0;
        }

        public bool RegeneratePaymentRequest(int id)
        {
            return dbContext.regeneratePaymentRequest(id) > 0;
        }
    }
}
