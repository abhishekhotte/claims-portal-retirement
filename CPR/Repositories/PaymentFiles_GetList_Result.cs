//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repositories
{
    using System;
    
    public partial class paymentFiles_GetList_Result
    {
        public Nullable<int> ChequeRunNumber { get; set; }
        public string DateIssued { get; set; }
        public string DateImported { get; set; }
        public string Status { get; set; }
        public Nullable<int> TotalCount { get; set; }
    }
}
