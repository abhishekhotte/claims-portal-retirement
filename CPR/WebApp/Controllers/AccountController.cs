﻿using BusinessLayer.BAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.Protocols;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class AccountController : Controller
    {
        public string Role = string.Empty, IpAddress = string.Empty, username = string.Empty;
        public int? RoleId = null, ActionId = null, EmployeeId;
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            string DomainName = WebConfigurationManager.AppSettings["DomainName"].ToString();
            DomainName = "flatworld.com";
            string employeeId, employeeName;
            if (!model.UserName.Contains('\\'))
            {
                model.UserName = "flatworld\\" + model.UserName;
            }

            if (ValidateADCredentials(model.UserName, model.Password))
            {
                var loginResult = new LoginBAL().ValidateCredentials(new Entities.Login { LoginId=model.UserName},out employeeId,out employeeName);
                if (loginResult)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    Response.Cookies["MCE_CPR_EmployeeId"].Value = employeeId;
                    Response.Cookies["MCE_CPR_EmployeeId"].Expires = DateTime.Now.AddMonths(2);

                    Response.Cookies["MCE_CPR_UserName"].Value = employeeName;
                    Response.Cookies["MCE_CPR_UserName"].Expires = DateTime.Now.AddMonths(2);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "You don't have permission to access this application");
                    return this.View(model);
                }
            }
            else
            {
                this.ModelState.AddModelError(string.Empty, "The user name or password provided is incorrect.");
                return this.View(model);
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Cookies["MCE_CPR_EmployeeId"].Expires = DateTime.UtcNow.AddDays(-1);
            Response.Cookies["MCE_CPR_UserName"].Expires = DateTime.UtcNow.AddDays(-1);
            return RedirectToAction("Login", "Account");
        }

       
        private bool ValidateADCredentials(string username, string password)
        {
            username = username.Substring(username.IndexOf('\\') + 1);
            NetworkCredential credentials = new NetworkCredential(username, password, ConfigurationManager.AppSettings["DomainName"].ToString());
            LdapDirectoryIdentifier id = new LdapDirectoryIdentifier(ConfigurationManager.AppSettings["DomainName"].ToString());

            using (LdapConnection connection = new LdapConnection(id, credentials, AuthType.Kerberos))
            {
                connection.SessionOptions.Sealing = true;
                connection.SessionOptions.Signing = true;
                try
                {
                    connection.Bind();
                }
                catch (LdapException lEx)
                {
                    WriteLogs.Write(lEx);
                    return false;
                }
            }
            return true;
        }

        [HttpPost]
        //[CustomAuthorize]
        public ActionResult CheckAuthentication()
        {
            if (!User.Identity.IsAuthenticated || Request.Cookies["MCE_CPR_EmployeeId"] == null || Request.Cookies["MCE_CPR_UserName"] == null)
                return Json(false, JsonRequestBehavior.AllowGet);
            else
                return Json(true, JsonRequestBehavior.AllowGet);

        }
    }
}