﻿#region NameSpaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;
#endregion
#region UI.Controllers
namespace CPR.UI.Controllers
{
    #region BaseController
    [CustomAuthorize]
    public class BaseController : Controller
    {
        
    } 
    #endregion
} 
#endregion