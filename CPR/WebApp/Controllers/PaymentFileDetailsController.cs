﻿using BusinessLayer.BAL;
using Common.Helpers;
using CPR.UI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class PaymentFileDetailsController : BaseController
    {
        // GET: PaymentDetails
        public ActionResult Index()
        {
            return PartialView("Index");
        }

        #region Get Payment Files list
        [HttpPost]
        public JsonResult GetList(GridSearchParameters param,int chequeRunNumber)
        {
            List<object> list = new PaymentFileDetailsBAL().GetList(param,chequeRunNumber);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion  
    }
}