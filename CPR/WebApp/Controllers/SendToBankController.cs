﻿using BusinessLayer.BAL;
using Common.Helpers;
using CPR.UI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class SendToBankController : BaseController
    {
        // GET: PaymentDetails
        public ActionResult Index()
        {
            return PartialView("Index");
        }

        #region Get Payment Files list
        [HttpPost]
        public JsonResult GetList(GridSearchParameters param, int chequeRunNumber)
        {
            List<object> list = new SendToBankBAL().GetList(param, chequeRunNumber);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region To cancel an individual payment request which has been exported to Bank
        [HttpPost]
        public JsonResult CancelPaymentRequest(int id)
        {
            return Json(new SendToBankBAL().CancelPaymentRequest(id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region To complete an individual payment request which has been exported to Bank
        [HttpPost]
        public JsonResult CompletePaymentRequest(int id, string bankingAppRef)
        {
            string completedBy = string.Empty;
            completedBy = Request.Cookies["MCE_CPR_UserName"] != null ? Request.Cookies["MCE_CPR_UserName"].Value : null;
            return Json(new SendToBankBAL().CompletePaymentRequest(id, completedBy, bankingAppRef), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region To export
        [HttpPost]
        public JsonResult ExportPaymentRequest(int id)
        {
            return Json(new SendToBankBAL().ExportPaymentRequest(id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region To renerate 
        [HttpPost]
        public JsonResult RegeneratePaymentRequest(int id)
        {
            return Json(new SendToBankBAL().RegeneratePaymentRequest(id), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}