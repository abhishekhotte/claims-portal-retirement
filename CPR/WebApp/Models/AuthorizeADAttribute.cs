﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace WebApp.Models
{
    public class AuthorizeADAttribute : AuthorizeAttribute
    {
        public string Groups { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (base.AuthorizeCore(httpContext))
            {
                ///* Return true immediately if the authorization is not 
                //locked down to any particular AD group */
                //if (String.IsNullOrEmpty(Groups))
                //    return true;
                //if (HttpContext.Current.Session["UserName"] == null)
                //{
                //    return false;
                //}

                string DomainName = WebConfigurationManager.AppSettings["DomainName"].ToString();

                if (DomainName == "flatworld.com")
                {
                    return true;
                }

                //  Groups = WebConfigurationManager.AppSettings["Roles"].ToString();

                // Get the AD groups
                var groups = Groups.Split(',').ToList();

                // Verify that the user is in the given AD group (if any)
                var context = new PrincipalContext(
                                      ContextType.Domain,
                                      DomainName);

                var userPrincipal = UserPrincipal.FindByIdentity(
                                       context,
                                       IdentityType.SamAccountName,
                                       httpContext.User.Identity.Name);

                foreach (var group in groups)
                    if (userPrincipal.IsMemberOf(context,
                         IdentityType.Name,
                         group))
                        return true;
            }
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            //if (HttpContext.Current.Session["UserName"] == null)
            //{
            //    filterContext.Result = new RedirectResult("~/Account/Login");
            //}

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                                 new RouteValueDictionary
                                 {
                                       { "action", "Index" },
                                       { "controller", "Home" }
                                 });
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }

    public class CustomAuthorize : ActionFilterAttribute
    {
        string RoleName = string.Empty;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity == null || !    filterContext.HttpContext.User.Identity.IsAuthenticated || 
                filterContext.HttpContext.Request.Cookies["MCE_CPR_EmployeeId"] == null || filterContext.HttpContext.Request.Cookies["MCE_CPR_UserName"] == null )
            {
                FormsAuthentication.SignOut();
                filterContext.HttpContext.Session.Abandon();
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.Cookies["MCE_CPR_EmployeeId"].Expires = DateTime.UtcNow.AddDays(-1);
                    filterContext.HttpContext.Response.Cookies["MCE_CPR_UserName"].Expires = DateTime.UtcNow.AddDays(-1);
                    filterContext.HttpContext.Response.StatusCode = 253;
                    filterContext.HttpContext.Response.End();
                }
                else
                {
                    filterContext.Result = new RedirectResult("#/account/login");
                }
            }
        }
    }
}