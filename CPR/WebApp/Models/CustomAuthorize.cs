﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Models
{
    public class CustomAuthorize: AuthorizeAttribute
    {
        #region CPSAuthorizeAttribute
        public CustomAuthorize()
        {
        }
        #endregion

        #region AuthorizeCore
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return LoggedUserInfo.GetCurrentSingleton().UserId > 0;
        }
        #endregion

        #region HandleUnauthorizedRequest
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (LoggedUserInfo.GetCurrentSingleton().UserId == 0)
            {


                filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary {
                                { "Controller", "Login" },
                                { "Action", "" }
                        });
                filterContext.HttpContext.Response.StatusCode = 253;
                //  filterContext.HttpContext.Response.End();
            }
        }
        #endregions
    }
}