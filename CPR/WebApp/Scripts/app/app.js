﻿var CPRApp = angular.module("CPRApp", ['ngMaterial', 'ngMessages', 'ngRoute', 'trNgGrid', 'ngLoadingSpinner']);
CPRApp.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: '../PaymentFiles/Index',
            controller: "PaymentFilesController"
        })
        .when("/payment-processing", {
            templateUrl: '../PaymentFiles/Index',
            controller: "PaymentFilesController"
        })
        .when("/payment-processing/payment-details", {
            templateUrl: '../PaymentFileDetails/Index',
            controller: "PaymentFileDetailsController"
        })
        .when("/payment-processing/send-to-bank", {
            templateUrl: '../SendToBank/Index',
            controller: "SendToBankController"
        })
        .when("/audit-tool", {
            templateUrl: '../AuditTool/Index',
            controller: "AuditToolController"
        })
        .when("/account/login", {
            templateUrl: '../Account/Login',
            controller: "LayoutController"
        })
        .when("/account/logout", {
            templateUrl: '../Account/Logout',
             controller: "LayoutController"
        })
        .otherwise({
            templateUrl: '../Home/PageNotFound',
        })
});

var StatusType = {
    SUCCESS: 0,
    ERROR: 1,
    WARNING: 2
}

var pCurrentPage = 0; //PageIndex
var pPageItems = 5;   //PageSize  
var pOrderBy = "";    //SortBy
var pOrderByReverse = false;  //SortDirection = 0

var PageSizeSelectedObj = { selected: 5 };

var DefaultDropDownValue = { selected: "Select" };
var errorMessage = '';
var getPageSizeList = function () {
    return [
               { value: 5, text: "5" },
               { value: 10, text: "10" },
               { value: 15, text: "15" },
               { value: 30, text: "30" },
               { value: 50, text: "50" },
               { value: 100, text: "100" }
    ];
}

