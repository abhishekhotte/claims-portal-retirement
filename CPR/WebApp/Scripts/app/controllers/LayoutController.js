﻿CPRApp.controller('LayoutController', function ($http, $scope, $location, modalService,  $mdDialog) {
  
    $mdDialog.show({
        contentElement: '#loginDialog',
                parent: angular.element(document.body),
                fullscreen: $scope.customFullscreen,
                clickOutsideToClose: false
            });

    $scope.closePopup = function () {
        $mdDialog.hide();
    }

});