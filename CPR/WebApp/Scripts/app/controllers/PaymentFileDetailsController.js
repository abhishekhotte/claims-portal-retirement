﻿CPRApp.controller('PaymentFileDetailsController', function ($http, $scope, $location, modalService, $mdDialog) {
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;
    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadPaymentFileDetailsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadPaymentFileDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadPaymentFileDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Ajax call  to fetch data from server...
    function LoadPaymentFileDetailsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        if (typeof (Storage) !== "undefined") {
            chequeRunNumber = localStorage.getItem("ChequeRunNumber");
            var temp = {
                currentPage: currentPage,
                pageItems: pageItems,
                filterBy: filterBy,
                filterByFields: angular.toJson(filterByFields),
                orderBy: orderBy,
                orderByReverse: orderByReverse,
            }
            var param = {
                param: temp,
                chequeRunNumber: chequeRunNumber
            }

            pCurrentPage = currentPage;
            pPageItems = pageItems;
            pOrderBy = orderBy;
            pOrderByReverse = orderByReverse
            var req = {
                method: 'POST',
                url: '/PaymentFileDetails/GetList?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: JSON.stringify(param)
            }
            $http(req).success(function (data) {
                $scope.PaymentFileDetails = data[0].ListOfItems;
                $scope.TotalRecordsCount = data[0].TotalRecordCount;
            }).error(function (error, status) {
                if (status < 0) {
                    modalService.show('Your session has expried', StatusType.ERROR);
                    setTimeout(function () {
                        window.location.href = "#/account/login";
                    }, 2000)
                }
                else
                    modalService.show('An error occured, please try again later', StatusType.ERROR);
            });
        }
    }

    $scope.gotoPaymentFiles = function () {
        var req = {
            method: 'POST',
            url: '/Account/CheckAuthentication?nocache=' + new Date().getTime(),
            contentType: "application/json",
        }
        $http(req).success(function (data) {
            if (data) {
                localStorage.setItem("ChequeRunNumber", gridItem.ChequeRunNumber);
                window.location.href = "#/payment-processing";
            }
            else {
                modalService.show('Your session has expried', StatusType.ERROR);
                setTimeout(function () {
                    window.location.href = "#/account/login";
                }, 2000)
            }
        }).error(function (error, status) {
            modalService.show('An error occured, please try again later', StatusType.ERROR);
        });
        
    }

    $scope.viewTransactionDetails = function (gridItem) {
        window.open("http://mceclaims_UAT/ice-claims/#payment_detail:CLAIM_ID:" + gridItem.ClaimReference + ",ID:" + gridItem.PaymentTransactionID);
    }

    $scope.viewClaimReferenceDetails = function (gridItem) {
        window.open("http://mceclaims_UAT/ice-claims/#claim_summary:CLAIM_ID:" + gridItem.ClaimReference);
    }
});


