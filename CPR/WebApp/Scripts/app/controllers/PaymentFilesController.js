﻿CPRApp.controller('PaymentFilesController', function ($http, $scope, $location, modalService, $mdDialog) {
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;
    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadPaymentFilesData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadPaymentFilesData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadPaymentFilesData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Ajax call  to fetch data from server...
    function LoadPaymentFilesData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        var param = {
            currentPage: currentPage,
            pageItems: pageItems,
            filterBy: filterBy,
            filterByFields: angular.toJson(filterByFields),
            orderBy: orderBy,
            orderByReverse: orderByReverse
        }
        pCurrentPage = currentPage;
        pPageItems = pageItems;
        pOrderBy = orderBy;
        pOrderByReverse = orderByReverse
        var req = {
            method: 'POST',
            url: '/PaymentFiles/GetList?nocache=' + new Date().getTime(),
            contentType: "application/json",
            data: JSON.stringify(param)
        }
        $http(req).success(function (data) {
            $scope.PaymentFiles = data[0].ListOfItems;
            $scope.TotalRecordsCount = data[0].TotalRecordCount;
        }).error(function (error, status) {
            if (status < 0) {
                modalService.show('Your session has expried', StatusType.ERROR);
                setTimeout(function () {
                    window.location.href = "#/account/login";
                }, 2000)
            }
            else
                modalService.show('An error occured, please try again later', StatusType.ERROR);
        });
    }

    $scope.sendToBank = function (gridItem) {
        if (typeof (Storage) !== "undefined") {
            var req = {
                method: 'POST',
                url: '/Account/CheckAuthentication?nocache=' + new Date().getTime(),
            }
            $http(req).success(function (data) {
                if (data) {
                    localStorage.setItem("ChequeRunNumber", gridItem.ChequeRunNumber);
                    window.location.href = "#/payment-processing/send-to-bank";
                }
                else {
                    modalService.show('Your session has expried', StatusType.ERROR);
                    setTimeout(function () {
                        window.location.href = "#/account/login";
                    }, 2000)
                }
            }).error(function (error, status) {
                  modalService.show('An error occured, please try again later', StatusType.ERROR);
            });
        }
    }

    $scope.viewPaymentDetails = function (gridItem) {
        if (typeof (Storage) !== "undefined") {
            var req = {
                method: 'POST',
                url: '/Account/CheckAuthentication?nocache=' + new Date().getTime(),
                contentType: "application/json",
            }
            $http(req).success(function (data) {
                if (data) {
                    localStorage.setItem("ChequeRunNumber", gridItem.ChequeRunNumber);
                    window.location.href = "#/payment-processing/payment-details";
                }
                else {
                    modalService.show('Your session has expried', StatusType.ERROR);
                    setTimeout(function () {
                        window.location.href = "#/account/login";
                    }, 2000)
                }
            }).error(function (error, status) {
                modalService.show('An error occured, please try again later', StatusType.ERROR);
            });
        }
    }

});


