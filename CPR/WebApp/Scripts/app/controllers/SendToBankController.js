﻿CPRApp.controller('SendToBankController', function ($http, $scope, $location, modalService, $mdDialog) {
    $scope.pageSizeList = getPageSizeList();
    $scope.pPageSizeObj = PageSizeSelectedObj;
    $scope.PageSize = $scope.pPageSizeObj.selected;

    //Called from on-data-required directive.
    $scope.onServerSideItemsRequested = function (currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        LoadPaymentTransactionDetailsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse);
    }

    // Called from global search box
    $scope.onChange = function () {
        LoadPaymentTransactionDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Called from page size drop down changed.
    $scope.changePageSize = function () {
        $scope.PageSize = $scope.pPageSizeObj.selected;
    }

    $scope.clearSearchParams = function () {
        $scope.filterBy = '';
        LoadPaymentTransactionDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
    }

    // Ajax call  to fetch data from server...
    function LoadPaymentTransactionDetailsData(currentPage, pageItems, filterBy, filterByFields, orderBy, orderByReverse) {
        if (typeof (Storage) !== "undefined") {
            chequeRunNumber = localStorage.getItem("ChequeRunNumber");
            var temp = {
                currentPage: currentPage,
                pageItems: pageItems,
                filterBy: filterBy,
                filterByFields: angular.toJson(filterByFields),
                orderBy: orderBy,
                orderByReverse: orderByReverse,
            }
            var param = {
                param: temp,
                chequeRunNumber: chequeRunNumber
            }

            pCurrentPage = currentPage;
            pPageItems = pageItems;
            pOrderBy = orderBy;
            pOrderByReverse = orderByReverse
            var req = {
                method: 'POST',
                url: '/SendToBank/GetList?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: JSON.stringify(param)
            }
            $http(req).success(function (data) {
                $scope.PaymentTransactionDetails = data.length>0 ? data[0].ListOfItems: [];
                $scope.TotalRecordsCount = data.length > 0 ? data[0].TotalRecordCount : 0;
                $scope.GBPCurrencyTotal = data.length > 0 ? data[1].GBPCurrencyTotal : '£ 0.00';
                $scope.EURCurrencyTotal = data.length > 0 ? data[1].EURCurrencyTotal : '€ 0.00';
            }).error(function (error, status) {
                if (status < 0) {
                    modalService.show('Your session has expried', StatusType.ERROR);
                    setTimeout(function () {
                        window.location.href = "#/account/login";
                    }, 2000)
                }
                else
                    modalService.show('Error occured while loading', StatusType.ERROR);
            }).catch()
        }
    }

    $scope.gotoPaymentFiles = function () {
        $http(req).success(function (data) {
            if (data) {
                localStorage.setItem("ChequeRunNumber", gridItem.ChequeRunNumber);
                window.location.href = "#/payment-processing";
            }
            else {
                modalService.show('Your session has expried', StatusType.ERROR);
                setTimeout(function () {
                    window.location.href = "#/account/login";
                }, 2000)
            }
        }).error(function (error, status) {
            modalService.show('An error occured, please try again later', StatusType.ERROR);
        });
    }

    //button click event for Regenerate/Export/Complete/cancel
    $scope.sendToBank = function (gridItem) {
        $scope.paymentId = gridItem.ID;
        if (gridItem.Status == 'Exported to Bank') { //payment has already exported, to complete or cancel the transaction 
            $scope.DateExported = gridItem.DateExported;
            $mdDialog.show({
                contentElement: '#cancelCompleteDialog',
                parent: angular.element(document.body),
                clickOutsideToClose: false
            });
        }
        else if (gridItem.Status == 'Complete') { //regenerate the payment batch
            $scope.chkConfirmRenerate = false;
            $mdDialog.show({
                contentElement: '#regenerateDialog',
                parent: angular.element(document.body),
                clickOutsideToClose: false
            });
        }
        else {//export the payment batch
            $scope.chkConfirmExport = false;
            $mdDialog.show({
                contentElement: '#exportDialog',
                parent: angular.element(document.body),
                clickOutsideToClose: false
            });
        }
    }

    $scope.cancelDialogPayment = function () {
        $scope.chkConfirmCancelled = false;
        $mdDialog.show({
            contentElement: '#cancelDialog',
            parent: angular.element(document.body),
            clickOutsideToClose: false
        });
        
    }

    $scope.cancelDialogConfirmPayment = function () {
        if (!$scope.chkConfirmCancelled) {
            modalService.show('Please select confirmation', StatusType.WARNING);
        }
        else {
            $scope.closePopup();
            var param = {
                id: $scope.paymentId
            }
            var req = {
                method: 'POST',
                url: '/SendToBank/CancelPaymentRequest?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: JSON.stringify(param)
            }
            $http(req).success(function (data) {
                if (data) {
                    modalService.show('Payment has been cancelled successfully', StatusType.SUCCESS);
                    LoadPaymentTransactionDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                }
            }).error(function (error, status) {
                if (status < 0) {
                    modalService.show('Your session has expried', StatusType.ERROR);
                    setTimeout(function () {
                        window.location.href = "#/account/login";
                    }, 2000)
                }
                else
                    modalService.show('Error occured while loading', StatusType.ERROR);
            });
        }
    }

    $scope.completeDialogPayment = function () {
        $scope.chkConfirmComplete = false;
        $scope.ApplicationReference = '';
        $scope.CompletePayment.$setPristine();
        $scope.CompletePayment.$setUntouched();
        $mdDialog.show({
            contentElement: '#completeDialog',
            parent: angular.element(document.body),
            clickOutsideToClose: false
        });
    }

    $scope.completeDialogCompletePayment = function () {
        if ($scope.CompletePayment.$valid) {
            if (!$scope.chkConfirmComplete) {
                modalService.show('Please select confirmation', StatusType.WARNING);
            }
            else {
                $scope.closePopup();
                var param = {
                    id: $scope.paymentId,
                    bankingAppRef: $scope.ApplicationReference
                }
                var req = {
                    method: 'POST',
                    url: '/SendToBank/CompletePaymentRequest?nocache=' + new Date().getTime(),
                    contentType: "application/json",
                    data: JSON.stringify(param)
                }
                $http(req).success(function (data) {
                    if (data) {
                        modalService.show('Payment has been completed successfully', StatusType.SUCCESS);
                        LoadPaymentTransactionDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                    }
                }).error(function (error, status) {
                    if (status < 0) {
                        modalService.show('Your session has expried', StatusType.ERROR);
                        setTimeout(function () {
                            window.location.href = "#/account/login";
                        }, 2000)
                    }
                    else
                        modalService.show('Error occured while loading', StatusType.ERROR);
                });
            }
        }
    }

    $scope.exportDialogConfirmPayment = function () {
        if (!$scope.chkConfirmExport) {
            modalService.show('Please select confirmation', StatusType.WARNING);
        }
        else {
            $scope.closePopup();
            var param = {
                id: $scope.paymentId
            }
            var req = {
                method: 'POST',
                url: '/SendToBank/exportPaymentRequest?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: JSON.stringify(param)
            }
            $http(req).success(function (data) {
                if (data) {
                    modalService.show('Payment has been exported to the bank', StatusType.SUCCESS);
                    LoadPaymentTransactionDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                }
            }).error(function (error, status) {
                if (status < 0) {
                    modalService.show('Your session has expried', StatusType.ERROR);
                    setTimeout(function () {
                        window.location.href = "#/account/login";
                    }, 2000)
                }
                else
                    modalService.show('Error occured while loading', StatusType.ERROR);
            });
        }
    }

    $scope.regenerateDialogConfirmPayment = function () {
        if (!$scope.chkConfirmRenerate) {
            modalService.show('Please select confirmation', StatusType.WARNING);
        }
        else {
            $scope.closePopup();
            var param = {
                id: $scope.paymentId
            }
            var req = {
                method: 'POST',
                url: '/SendToBank/regeneratePaymentRequest?nocache=' + new Date().getTime(),
                contentType: "application/json",
                data: JSON.stringify(param)
            }
            $http(req).success(function (data) {
                if (data) {
                    modalService.show('Payment has been regenerated to the bank', StatusType.SUCCESS);
                    LoadPaymentTransactionDetailsData(pCurrentPage, pPageItems, $scope.filterBy, {}, pOrderBy, pOrderByReverse);
                }
            }).error(function (error, status) {
                if (status < 0) {
                    modalService.show('Your session has expried', StatusType.ERROR);
                    setTimeout(function () {
                        window.location.href = "#/account/login";
                    }, 2000)
                }
                else
                    modalService.show('Error occured while loading', StatusType.ERROR);
            });
        }
    }

    $scope.closePopup = function () {
        $mdDialog.hide();
    }

    $scope.viewClaimReferenceDetails = function (gridItem) {
        window.open("http://mceclaims_UAT/ice-claims/#claim_summary:CLAIM_ID:" + gridItem.ClaimReference);
    }
});


