﻿CarParkingSystemApp.directive('uniqueValue', function ($timeout, $q, $http) {
    return {
        restrict: 'EA',
        require: 'ngModel',
        scope: "=",
        link: function (scope, elm, attr, model) {
            elm.bind('blur', function () {
                if (elm.val().trim().length != 0) {
                    scope.$apply(function () {
                        var vEntity = {
                            TableType: scope.TableType,
                            Column: getColumnName(model.$name),
                            Value: elm.val(),
                            Id: scope.Id
                        }
                        var req = {
                            method: 'POST',
                            url: getBaseUrl() + '/User/CheckValueExists?nocache=' + new Date().getTime(),
                            contentType: "application/json",
                            data: vEntity
                        }
                        $http(req).success(function (data) {
                            model.$setValidity('uniqueValue', !data);
                            if (vEntity.Column == 'LoginId') {
                                return scope.LoginId;
                            }
                            else if (vEntity.Column == 'EmailId') {
                                return scope.EmailId;
                            }
                            else if (vEntity.Column == 'Mobile') {
                                return scope.Mobile;
                            }
                            else if (vEntity.Column == 'TariffName') {
                                return scope.TariffName;
                            }
                            else if (vEntity.Column == 'VehicleNumber') {
                                return scope.VehicleNumber;
                            }
                        });
                    });
                }
            })
        }
    }

    function getColumnName(name) {
        if (name == 'LoginId')
            return 'LoginId';
        else if (name == 'EmailId')
            return 'EmailId'
        else if (name == 'Mobile')
            return 'Mobile'
        else if (name == 'TariffName')
            return 'TariffName'
        else if (name == 'VehicleNumber')
            return 'VehicleNumber'
    }
})