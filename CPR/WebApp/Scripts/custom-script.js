﻿
$(document).ready(function () {
    $(".mandatory-field").blur(function () { //triggers event for a selected control if the focus comes out
        fieldValue = $(this).val(); 
        if (fieldValue == '' || fieldValue == -1) //add error validation class if the value is empty for text field or -1 for dropdownlist 
            $(this).addClass('txt-error');
        else  //removes validation if it is valid
            $(this).removeClass('txt-error');
    });

    //immediately hides the status message if modal cancel button clicks
    $("[data-dismiss]").click(function () {
        hideStatus();
    });

    $(".treeview-menu li a").hover(function () {
        $(this).attr("title",$(this).text())
    })

    //$(document).ajaxStart(function () {
    //    alert(1);
    //});
    //$(document).ajaxStop(function () {
    //   alert(2);
    //});

});



function validateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email); //returns true if it is valid email else returns false
}

function showSuccessStatus(msg) {
    $("#iconType").removeClass().addClass("fa fa-check");
    $("#statusMsg").removeClass().addClass('success-status').show().delay(3000).fadeOut("slow"); //shows success status message by adding a success-status class and fade it after 3 seconds
    $("#statusText").text(msg); // success status message
}

function showWarningStatus(msg) {
    $("#iconType").removeClass().addClass("fa fa-warning");
    $("#statusMsg").removeClass().addClass('warning-status').show().delay(3000).fadeOut("slow"); //shows success status message by adding a warning-status class and fade it after 3 seconds
    $("#statusText").text(msg); // warning status message
}

function showStatus(msg) {
    $("#iconType").removeClass().addClass("fa fa-spinner fa-spin");
    $("#statusMsg").removeClass().addClass("normal-status").show(); //shows status message by adding a normal-status class, use this method to process Ajax request to display waiting status
    $("#statusText").text(msg); //status message
}

function hideStatus() {
    $("#statusMsg").hide(); //use this method to hide waiting status message after completion of Ajax request
}

function autohideStatus(msg) {
    $("#statusMsg").removeClass().addClass('normal-status').show().delay(3000).fadeOut("slow");
    $("#statusText").text(msg);
}

//checks if the any input field is empty or not
function checkIfFieldEmpty() {
    $(".mandatory-field").each(function () { //loop through all input fields (textboxes and dropdownlists)
        fieldValue = $(this).val();
        if (fieldValue == '' || fieldValue == -1) //if the control is textbox then the value is text else if it is dropdownlist the selected value is -1
            $(this).addClass('txt-error'); //Validation failed(add a validation css for the control)
        else
            $(this).removeClass('txt-error'); //Validation success(remove validation css for the control)
    })
}

function clearInputFields(id) {
    $('#'+id+' input:text, textarea').val(''); //clears the textbox and textarea value 
    $('#'+id+' .mandatory-field').removeClass('txt-error'); //removes the validation border(red) for all mandatory-field controls
    $('#'+id+' select').val(-1); //make the dropdownlist selection to -1
}

//allow user to enter only numbers
function allowOnlyNumbers(e) {
    e = (e) ? e : window.event;
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false; //ignore the characters if the user presses other than numbers
    return true; //accepts the characters if the user presses numbers(0-9)
}

//check if the value is decimal or not
function validateDecimal(value) {
    var RE = /^\d*\.?\d*$/;
    if (RE.test(value))
        return true; //if it is decimal value
    else
        return false; //if it is not decimal
}

function checkvalueExistsInArray(value, array) {
    return array.indexOf(value) > -1;
}

//Reload the Kendo Grid
function reloadKendoGrid(gridId) { //pass Grid Id as a parameter 
    var grid = $("#" + gridId).data("kendoGrid");
    grid.dataSource.page(1); //set Grid pagination number to 1
    grid.dataSource.read(); //refresh the Grid
}

function kendoGridCustomIcons() {
    $(".k-grid-cancel").html("").removeClass("k-button k-button-icontext k-primary").addClass("fa fa-times k-grid-custom-icons").attr('title', 'Cancel');
    $(".k-grid-update").html("").removeClass("k-button k-button-icontext k-primary").addClass("fa fa-check k-grid-custom-icons").attr('title', 'Update');
    $(".k-grid-edit").html("").removeClass("k-button k-button-icontext k-primary").addClass("fa fa-pencil-square-o k-grid-custom-icons").attr('title', 'Edit');
    $(".k-grid-Download").html("").removeClass("k-button k-button-icontext k-primary").addClass("fa fa-download k-grid-custom-icons").attr('title', 'Download');
    $(".k-grid-view").html("").removeClass("k-button k-button-icontext k-primary").addClass("fa fa-eye k-grid-custom-icons").attr('title', 'View');
}

function clearKendoGridEditing(gridId) {
    $("#"+gridId).data("kendoGrid").cancelChanges();
}

